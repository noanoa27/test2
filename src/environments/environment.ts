// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyD9m_T3CaS7cinqrK7_6llgNzpPH9ROX98",
    authDomain: "testa-ex.firebaseapp.com",
    databaseURL: "https://testa-ex.firebaseio.com",
    projectId: "testa-ex",
    storageBucket: "testa-ex.appspot.com",
    messagingSenderId: "67484582737"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
